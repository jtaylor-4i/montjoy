import React, { useState, useRef } from 'react';
import { View, StyleSheet, Button } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import CarouselCardItem, { SLIDER_WIDTH, ITEM_WIDTH } from "./CarouselCardItem";
import data from './card-content';


const CarouselCard = (props) => {
    const [index, setIndex] = React.useState(0);
    const isCarousel = React.useRef(null);

    return (
        <>
            <Carousel
                layout="tinder"
                layoutCardOffset={3}
                ref={isCarousel}
                data={data}
                renderItem={CarouselCardItem}
                sliderWidth={SLIDER_WIDTH}
                itemWidth={ITEM_WIDTH}
                onSnapToItem={(index) => setIndex(index)}
                useScrollView={true}
            />
            <Button title='Sign Up Today' onPress={props.navigate}/>
            <Pagination
                dotsLength={data.length}
                activeDotIndex={index}
                carouselRef={isCarousel}
                dotStyle={{
                    width: 10,
                    height: 5,
                    borderRadius: 5,
                    marginHorizontal: 0,
                    backgroundColor: "#fff",
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
                tappableDots={true}
            />
        </>
    );
}


export default CarouselCard;
