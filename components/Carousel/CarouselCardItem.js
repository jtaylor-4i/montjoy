import React from 'react'
import { View, Text, StyleSheet, Dimensions, Image } from "react-native";
import { Title } from 'react-native-paper';

export const SLIDER_WIDTH = Dimensions.get('window').width + 80
export const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9)

const CarouselCardItem = ({ item, index }) => {
    return (
        <View style={styles.container} key={index}>
           <Image
                source={{ uri: item.imgUrl }}
                style={styles.image}
                resizeMode='contain'
                backgroundColor='#031116'

            />
            <Title style={styles.header}>{item.title}</Title>
            <View style={styles.bodyContainer}>
            <Text style={styles.body}>{item.body}</Text>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 0.8,
        backgroundColor: 'white',
        borderRadius: 8,
        width: ITEM_WIDTH,
        shadowColor: "#fff",
        shadowOffset: {
            width: 5,
            height: 8,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
        
    },
    image: {
        width: ITEM_WIDTH,
        height: '60%',
    },
    header: {
        color: "#222",
        fontSize: 28,
        fontWeight: "bold",
        paddingLeft: 20,
        paddingTop: 20,
        paddingBottom: 20,
    },
    body: {
        color: "#222",
        fontSize: 18,
        paddingLeft: 20,
        paddingRight: 20
    },
    
})

export default CarouselCardItem;
