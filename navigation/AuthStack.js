import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import SignupScreen from '../screens/InitialFlow/Signup';
import LoginScreen from "../screens/MainFlow/Login";
import Onboarding from '../screens/InitialFlow/Onboarding';
const Stack = createStackNavigator();

export default function AuthStack() {
    return (
        <Stack.Navigator
            initialRouteName="Onboarding"
            screenOptions={ {
                headerTitleAlign: 'center',
                headerStyle: {
                    backgroundColor: '#3740FE',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            } }>
            <Stack.Screen
                name="Signup"
                component={ SignupScreen }
                options={ { title: 'Signup' } }
            />
            <Stack.Screen
                name="Login"
                component={ LoginScreen }
                options={
                    { title: 'Login' },
                    { headerLeft: null }
                }
            />
            <Stack.Screen
                name="Onboarding"
                component={ Onboarding }
                options={
                    { title: 'Onboarding' },
                    { headerLeft: null }
                }
            />
        </Stack.Navigator>
    );
}