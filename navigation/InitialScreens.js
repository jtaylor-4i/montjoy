import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import OnboardingScreen from '../screens/InitialFlow/Onboarding';
import HomeScreen from '../screens/InitialFlow/HomeScreen';
import LandingScreen from '../screens/InitialFlow/Onboarding';
import RegisterScreen from '../screens/InitialFlow/Register';
import AuthStack from './AuthStack';
import LoginScreen from '../screens/MainFlow/Login';
import SignupScreen from '../screens/InitialFlow/Signup';
const Stack = createStackNavigator();


const InitialStackNavigation = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName='Onboarding' headerMode='screen'>
                <Stack.Screen
                    name="Onboarding"
                    component={ OnboardingScreen }
                    options={ { headerShown: false } }
                />
                <Stack.Screen
                    name="Home"
                    component={ HomeScreen }
                    options={ { headerShown: false } }
                />
                <Stack.Screen
                    name="Auth"
                    component={ AuthStack }
                    options={ { headerShown: false } }
                />
                <Stack.Screen
                    name='Login'
                    component={ LoginScreen }
                    options={
                        { headerShown: true }
                    }
                />
                <Stack.Screen
                    name='Signup'
                    component={ SignupScreen }
                    options={
                        { headerShown: true }
                    }
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default InitialStackNavigation;
