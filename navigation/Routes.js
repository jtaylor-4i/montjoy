import React, { useContext, useState, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import auth from '@react-native-firebase/auth';
import { AuthContext } from '../src/context/AuthProvider';
import AuthStack from "./AuthStack";
import InitialScreens from "./InitialScreens";
import Loading from '../components/Loading';


export default function Routes() {
    const { user, setUser } = useContext(AuthContext);
    const [loading, setLoading] = useState(true);
    const [initializing, setInitializing] = useState(true);

    function onAuthStateChanged(user) {
        setUser(user);
        if(initializing) setInitializing(false);
        setLoading(false);
    }

    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber;
    }, []);

    if(loading) {
        return <Loading />;
    }

    return (
        <NavigationContainer>
            {user ? <InitialScreens /> : <AuthStack />}
        </NavigationContainer>
    );
}