import React from 'react';
import { AuthProvider } from "../src/context/AuthProvider";
import Routes from './Routes';

export default function Provider() {
    return (
        <AuthProvider>
            <Routes />
        </AuthProvider>
    );
}

