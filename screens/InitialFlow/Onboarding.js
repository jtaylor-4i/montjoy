import React from "react";
import { SafeAreaView, View, StyleSheet, Image,  } from "react-native";
import { Title, Button } from 'react-native-paper';

import CarouselCard from "../../components/Carousel/CarouselCard";


const Onboarding = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.screen}>
      <Title style={styles.titleText}>Be Anwyehere!!</Title>
      <View style={styles.cardContainer}>
      <CarouselCard 
        navigate={() => navigation.navigate('Signup')}
      />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "#031116",
    justifyContent: 'center',
    alignItems: 'center',

  },
  cardContainer: {
    paddingTop: 10,
  },
  titleText: {
    color: '#fff',
    marginTop: 40,
  }
});

export default Onboarding;
