import React, { useState, useDispatch } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { TextInput, Headline, Button } from 'react-native-paper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import firebase from '../../database/firebase';


const Register = ( { navigation } ) => {
    const [ fullName, setFullName ] = useState( '' );
    const [ email, setEmail ] = useState( '' );
    const [ password, setPassword ] = useState( '' );
    const [ confirmPassword, setConfirmPassword ] = useState( '' );


    return (
        <View style={ styles.container }>
            <View style={ styles.headerContainer }>
                <Headline>Free to sign up</Headline>
            </View>
            <KeyboardAwareScrollView
                style={ { flex: 1, width: '100%' } }
                keyboardShouldPersistTaps="always"
            >
                <View>
                    <TextInput
                        label='Full Name'
                        value={fullName}
                        right={ <TextInput.Icon name='account-arrow-left-outline' /> }
                        onChangeText={ setFullName }
                    />
                </View>
                <View>
                    <TextInput
                        label='E-mail'
                        value={email}
                        right={ <TextInput.Icon name='at' /> }
                        onChangeText={ setEmail }

                    />
                </View>
                <View>
                    <TextInput
                        label='Create Password'
                        value={password}
                        secureTextEntry
                        right={ <TextInput.Icon name="form-textbox-password" /> }
                        onChangeText={ setPassword }
                    />
                </View>
                <View>
                    <TextInput
                        label='Confirm Password'
                        value={confirmPassword}
                        secureTextEntry
                        right={ <TextInput.Icon name="lock-check" /> }
                        onChangeText={ setConfirmPassword }
                        blurOnSubmit={ true }
                    />
                </View>
            </KeyboardAwareScrollView>
            <View>
                <Text>{ fullName }</Text>
                <Button />
            </View>
        </View>
    )
}


const styles = StyleSheet.create( {
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 30
    },
    headerContainer: {
        marginTop: 30,
    },
    logo: {
        flex: 1,
        height: 120,
        width: 90,
        alignSelf: "center",
        margin: 30
    },
    input: {
        height: 48,
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: 'white',
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        paddingLeft: 16
    },
    button: {
        backgroundColor: '#788eec',
        marginLeft: 30,
        marginRight: 30,
        marginTop: 20,
        height: 48,
        borderRadius: 5,
        alignItems: "center",
        justifyContent: 'center'
    },
    buttonTitle: {
        color: 'white',
        fontSize: 16,
        fontWeight: "bold"
    },
    footerView: {
        flex: 1,
        alignItems: "center",
        marginTop: 20
    },
    footerText: {
        fontSize: 16,
        color: '#2e2e2d'
    },
    footerLink: {
        color: "#788eec",
        fontWeight: "bold",
        fontSize: 16
    }
} );
export default Register;
