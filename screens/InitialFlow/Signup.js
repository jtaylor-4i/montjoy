// components/signup.js

import React, { Component } from 'react';
import { StyleSheet, Text, View, Alert, ActivityIndicator, KeyboardAvoidingView } from 'react-native';
import { TextInput, Headline, Button } from 'react-native-paper';
import firebase from '../../database/firebase';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


export default class Signup extends Component {

    constructor () {
        super();
        this.state = {
            displayName: '',
            email: '',
            password: '',
            isLoading: false
        }
    }

    updateInputVal = ( val, prop ) => {
        const state = this.state;
        state[ prop ] = val;
        this.setState( state );
    }

    registerUser = () => {
        if ( this.state.email === '' && this.state.password === '' ) {
            Alert.alert( 'Enter details to signup!' )
        } else {
            this.setState( {
                isLoading: true,
            } )
            firebase
                .auth()
                .createUserWithEmailAndPassword( this.state.email, this.state.password )
                .then( ( res ) => {
                    res.user.updateProfile( {
                        displayName: this.state.displayName
                    } )
                    console.log( 'User registered successfully!' )
                    this.setState( {
                        isLoading: false,
                        displayName: '',
                        email: '',
                        password: ''
                    } )
                    this.props.navigation.navigate( 'Home' )
                } )
                .catch( error => this.setState( { errorMessage: error.message } ) )
        }
        
    }

    render() {
        if ( this.state.isLoading ) {
            return (
                <View style={ styles.preloader }>
                    <ActivityIndicator size="large" color="#9E9E9E" />
                </View>
            )
        }
        return (
            <View style={ styles.container }>
                <Headline style={styles.headerTitle}>Free to signup</Headline>
                    <KeyboardAvoidingView
                style={ { flex: 1, width: '100%' } }
                keyboardShouldPersistTaps="always"
            >
                <TextInput
                    style={ styles.inputStyle }
                    label='Create a username'
                    value={ this.state.displayName }
                    onChangeText={ ( val ) => this.updateInputVal( val, 'displayName' ) }
                    right={ <TextInput.Icon name='account-arrow-left-outline' /> }
                />
                <TextInput
                    style={ styles.inputStyle }
                    label="Email"
                    value={ this.state.email }
                    onChangeText={ ( val ) => this.updateInputVal( val, 'email' ) }
                    right={ <TextInput.Icon name='at' /> }
                />
                <TextInput
                    style={ styles.inputStyle }
                    label="Password"
                    value={ this.state.password }
                    onChangeText={ ( val ) => this.updateInputVal( val, 'password' ) }
                    maxLength={ 15 }
                    secureTextEntry={ true }
                    right={ <TextInput.Icon name="form-textbox-password" /> }
                />
                <TextInput
                    style={ styles.inputStyle }
                    label="Confirm Password"
                    value={ this.state.password }
                    onChangeText={ ( val ) => this.updateInputVal( val, 'password' ) }
                    maxLength={ 15 }
                    secureTextEntry={ true }
                    right={ <TextInput.Icon name="lock-check" /> }
                    
                />
                    <Button
                    color="#fff"
                    onPress={ () => this.registerUser() }
                >
                    Signup 
                </Button>
                    <Text
                    style={ styles.loginText }
                    onPress={ () => this.props.navigation.navigate( 'Login' ) }>
                    Already Registered? Click here to login
                </Text>
                </KeyboardAvoidingView>
            </View>
        );
    }
}

const styles = StyleSheet.create( {
    container: {
        flex: 1,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        padding: 5,
        backgroundColor: "#031116",
    },
    inputStyle: {
        width: '100%',
        marginBottom: 15,
        paddingBottom: 15,
        alignSelf: "center",
        borderColor: "#fff",
        borderWidth: 5
    },
    loginText: {
        color: '#fff',
        fontSize: 16,
        marginTop: 10,
        textAlign: 'center'
    },
    preloader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    headerTitle: {
        color: '#fff',
        textAlign: 'center',
        paddingBottom: 5,
    },
} );