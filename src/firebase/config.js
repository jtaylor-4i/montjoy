import * as firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyA4dmIrOg3f-NUuRkWundqzd02kQyuq88Q',
  authDomain: 'montjoy-app-d62dc.firebaseapp.com',
  databaseURL: 'https://montjoy-app-d62dc.firebaseio.com',
  projectId: 'montjoy-app-d62dc',
  storageBucket: 'gs://montjoy-app-d62dc.appspot.com',
  messagingSenderId: '1048509925139',
  appId: '1:1048509925139:android:70d4c39039a19613febce6',
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export { firebase };