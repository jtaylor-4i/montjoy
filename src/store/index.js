import { createStore } from 'redux';
import usersReducer from '../reducers';


export default store = createStore(usersReducer);

